<?php
/**
 * The template for displaying page NOT FOUND.
 *
 * @package The Same
 */
get_header(); ?>
<section id="content">
    <div class="wrapper page_text">
        <h1 class="page_title"><?php the_category(); ?></h1>
        <div class="columns">
            <div class="column column75">
                <article class="article">
                    <div class="article_image nomargin">
                        <h2><?php _e( 'Not found', 'thesame' ); ?></h2>
                        <p><?php _e( 'Sorry, but you are looking for something that isn\'t here.', 'thesame' ); ?></p>
                </article>
            </div>
            <?php  get_sidebar(); ?>
        </div>
</section>

</div>
</div>
<?php get_footer(); ?>
