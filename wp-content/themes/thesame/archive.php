<?php
/**
 * The template for displaying archive
 *
 *
 * @package The Same
 */
get_header(); ?>
		<div id="content">
			<div class="container">
				<div class="shadow_block">
					<div class="inner_page clearfix sidebar_right">
						<div class="page_section">
							<div class="gutter">
							    <h2><?php
								if ( is_day() ) :
									printf( __( 'Daily Archives: %s', 'thesame' ), '<span>' . get_the_date() . '</span>' );
								elseif ( is_month() ) :
									printf( __( 'Monthly Archives: %s', 'thesame' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'thesame' ) ) . '</span>' );
								elseif ( is_year() ) :
									printf( __( 'Yearly Archives: %s', 'thesame' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'thesame' ) ) . '</span>' );
								else :
									_e( 'Archives', 'thesame' );
								endif;
								?></h2><hr>
								<?php while (have_posts()) : the_post(); ?>
									<?php get_template_part( 'content', 'posts');  ?>
								<?php endwhile; ?>
                                <div class="underline"></div>
                                <?php
                                $prev_link = get_previous_posts_link(__('Next posts', 'thesame'));
                                $next_link = get_next_posts_link(__('Previous Posts', 'thesame'));
                                // as suggested in comments
                                if ($prev_link || $next_link) { ?>
                                    <nav class="pagination">
                                        <span class="pagi-prev"><?php echo $prev_link; ?></span>
                                        <span class="pagi-next"><?php echo $next_link; ?></span>
                                    </nav>
                                <?php } ?>
							</div>
						</div>
						<?php  get_sidebar(); ?>
					</div>
				</div>
			</div>
		</div>
<?php get_footer(); ?>