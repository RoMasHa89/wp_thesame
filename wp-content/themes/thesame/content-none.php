<?php
/**
 * The template for displaying page Search: NOT FOUND.
 *
 * @package The Same
 */
?>
<h2><?php _e( 'Not found', 'thesame' ); ?></h2>
<p><?php _e( 'Sorry, but you are looking for something that isn\'t here.', 'thesame' ); ?></p>
