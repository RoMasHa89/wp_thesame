<?php
/**
 *
 * @package The Same
 */
?>
<article class="article">
    <div class="article_image nomargin">
        <div class="inside">
            <?php the_post_thumbnail('blog-thumbnail-thesame') ?>
        </div>
    </div>
    <div class="article_details">
        <ul class="article_author_date">
            <li><em><?php _e('Add:', 'thesame') ?> </em><?php the_time( get_option( 'date_format' ) ); ?></li>
            <li><em><?php _e('Author:', 'thesame') ?> </em> <?php the_author_link();?></li>
        </ul>
        <p class="article_comments"><em><?php _e('Comment:', 'thesame') ?></em> <?php echo get_comments_number(); ?></p>
    </div>
    <h1><?php the_title(); ?></h1>
    <?php if ($quote = get_field('quote')) : ?>
        <q><?php echo $quote; ?></q>
    <?php endif; ?>
    <p><?php the_excerpt(); ?></p>
    <a class="button button_small button_orange float_left" href="<?php echo get_permalink(get_the_ID()); ?>"><span class="inside"><?php _e('Read More', 'thesame'); ?></span></a>
</article>
