<?php
/**
 * The template for displaying the footer.
 *
 *
 * @package The Same
 */
?>
                </div>
            </div>
            <div id="page_bottom">
                <!-- BEGIN ABOVE_FOOTER -->
                <section id="above_footer">
                    <div class="wrapper above_footer_boxes page_text">
                        <?php if ( is_active_sidebar('footer-widget-area') ) : ?>
                            <?php dynamic_sidebar('footer-widget-area'); ?>
                        <?php endif; ?>
                    </div>
                </section>
                <!-- END ABOVE_FOOTER -->
                <!-- BEGIN FOOTER -->
                <footer id="footer">
                    <div class="wrapper">
                        <?php
                            if ($copyright = get_theme_mod( 'copyright', '' )) {
                                $copyright = str_replace("[link]", "<a href='" . get_home_url() ."'>" . get_bloginfo( 'name' ) . "</a>", $copyright);
                                $copyright = str_replace("[year]", date("Y"), $copyright);
                            } else {
                                $copyright = __( 'Copyright', 'thesame' ) . "&copy; " . date("Y") .  "<a href='" . get_home_url() . "'> " . get_bloginfo( 'name' ) . "</a> - " . __( 'All Rights Reserved', 'thesame' );
                            }
                        ?>
                        <p class="copyrights"><?php echo $copyright; ?></p>

                        <a href="#page" class="up">
                            <span class="arrow"></span>
                            <span class="text">top</span>
                        </a>
                    </div>
                </footer>
                <!-- END FOOTER -->
        </div>
        <?php wp_footer(); ?>
    </body>
</html>