<?php
/**
 * The template for displaying home page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage wp-themes
 */
get_header();

    if ( have_posts() ) {
        the_post();
        the_content();
    } else {
        get_template_part( 'content', 'none' );
    }

get_footer(); ?>
<!-- Number of queries: <?php echo get_num_queries(); ?> -->