<?php
/**
 * The Same functions and definitions
 *
 * @package The Same
 */

// Require a classes file
require_once(get_template_directory() . '/classes.php');


// Custom Breadcrumbs
function custom_breadcrumbs() {

    // Settings
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs';
    $home_title         = 'Homepage';

    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = '';

    // Get the query & post information
    global $post,$wp_query;

    // Do not display on the homepage
    if ( !is_front_page() ) {

        // Build the breadcrums
        echo '<div id="' . $breadcrums_id . '" class="' . $breadcrums_class . '"><div class="inside">';

        // Home page
        echo '<a class="first bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '"><span>' . $home_title . '</span></a>';

        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {

            echo '<a href="#"><span class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></span></a>';

        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {

            // If post is a custom post type
            $post_type = get_post_type();

            // If it is a custom post type display name and link
            if($post_type != 'post') {

                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);

                echo '<a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '"><span>' . $post_type_object->labels->name . '</span></a>';

            }

            $custom_tax_name = get_queried_object()->name;
            echo '<a href="#"><span class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></span>';

        } else if ( is_single() ) {

            // If post is a custom post type
            $post_type = get_post_type();

            // If it is a custom post type display name and link
            if($post_type != 'post') {
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
                echo '<a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '"><span>' . $post_type_object->labels->name . '</span></a>';
            }
            // Get post category info
            $category = get_the_category();

            if(!empty($category)) {
                // Get last category post is in
                $last_category = end(array_values($category));
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);

                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';

                foreach($cat_parents as $parents) {
                    preg_match_all('/<a[^>]+href=([\'"])(.+?)\1[^>]*>/i', $parents, $link);
                    $name = strip_tags($parents);
                    $parents = "<a href='".$link[2][0]."'><span>$name</span></a>";
                    $cat_display .= $parents;
                }
            }
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
            }
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<a href="#"><span class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></span></a>';
                // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                echo '<a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '"><span>' . $cat_name . '</span></a>';
                echo '<a href="#"><span class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></span></a>';
            } else {
                echo '<a href="#"><span class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></span></a>';
            }
        } else if ( is_category() ) {
            // Category page
            echo '<a href="#"><span class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></span></a>';
        } else if ( is_page() ) {
            // Standard page
            if( $post->post_parent ){
                // If child page, get parents
                $anc = get_post_ancestors( $post->ID );
                // Get parents in the right order
                $anc = array_reverse($anc);
                // Parent page loop
                foreach ( $anc as $ancestor ) {
                    $parents .= '<a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '"><span>' . get_the_title($ancestor) . '</span></a>';
                }
                // Display parent pages
                echo $parents;
                // Current page
                echo '<a href="#"><span class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></span></a>';
            } else {
                // Just display current page if not parents
                echo '<a href="#"><span class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></span></a>';
            }
        } else if ( is_tag() ) {
            // Tag page
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
            // Display the tag name
            echo '<a href="#"><span class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></span></a>';

        } elseif ( is_day() ) {
            // Day archive
            // Year link
            echo '<a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '"><span>' . get_the_time('Y') . ' Archives</span></a>';
            // Month link
            echo '<span class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '"><span>' . get_the_time('M') . ' Archives</span></a></span>';
            // Day display
            echo '<a href="#"><span class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></span></a>';

        } else if ( is_month() ) {
            // Month Archive
            // Year link
            echo '<a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '"><span>' . get_the_time('Y') . ' Archives</span></a>';
            // Month display
            echo '<a href="#"><span class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></span></a>';

        } else if ( is_year() ) {
            // Display year archive
            echo '<a href="#"><span class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></span></a>';
        } else if ( is_author() ) {
            // Author archive
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
            // Display author name
            echo '<a href="#"><span class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></span></a>';
        } else if ( get_query_var('paged') ) {
            // Paginated archives
            echo '<a href="#"><span class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></span></a>';
        } else if ( is_search() ) {
            // Search results page
            echo '<a href="#"><span class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></span></a>';
        } elseif ( is_404() ) {
            // 404 page
            echo '<a href="#"><span>' . 'Error 404' . '</span></a>';
        }
        echo '</div></div>';
    }
}



// Enqueue styles
function enqueue_styles() {
    global $wp_styles;

	wp_enqueue_style( 'reset-style', get_template_directory_uri() . '/css/reset.css', array());
    wp_register_style( 'light-style', get_template_directory_uri() . '/css/light.css', array());
    wp_register_style( 'dark-style', get_template_directory_uri() . '/css/dark.css', array());
	wp_enqueue_style( 'flexslider-style', get_template_directory_uri() . '/css/flexslider.css');
	wp_enqueue_style( 'prettyPhoto-style', get_template_directory_uri() . '/css/prettyPhoto.css');

    $wp_styles->add_data( 'light-style', 'title', 'light' );
    $wp_styles->add_data( 'dark-style', 'title', 'dark' );

    wp_enqueue_style('dark-style');
    wp_enqueue_style('light-style');

}
add_action('wp_enqueue_scripts', 'enqueue_styles');

// Enqueue scripts
function enqueue_scripts () {
	wp_register_script('jquery-min', get_template_directory_uri() . '/js/jquery.min.js', $in_footer = true);
	wp_enqueue_script('jquery-min');

	wp_register_script('jquery-ui-min', get_template_directory_uri() . '/js/jquery.ui.min.js', $in_footer = true);
	wp_enqueue_script('jquery-ui-min');

	wp_register_script('jquery.flexslider', get_template_directory_uri() . '/js/jquery.flexslider.min.js', $in_footer = true);
	wp_enqueue_script('jquery.flexslider');

    wp_register_script('jquery.quicksand', get_template_directory_uri() . '/js/jquery.quicksand.js', $in_footer = true);
    wp_enqueue_script('jquery.quicksand');

	wp_register_script('prettyphoto', get_template_directory_uri() . '/js/jquery.prettyphoto.min.js', $in_footer = true);
	wp_enqueue_script('prettyphoto');

	wp_register_script('jquery.stylesheettoggle', get_template_directory_uri() . '/js/jquery.stylesheettoggle.js', $in_footer = true);
	wp_enqueue_script('jquery.stylesheettoggle');

	wp_register_script('onload', get_template_directory_uri() . '/js/onload.js', $in_footer = true);
	wp_enqueue_script('onload');
}
add_action('wp_enqueue_scripts', 'enqueue_scripts');


// Setup theme options
if ( ! function_exists( 'thesame_setup' ) ) :
function thesame_setup() {
	load_theme_textdomain( 'thesame', get_template_directory() . '/languages' );

	register_nav_menu( 'main', __( 'Main Menu', "thesame" ) );

	add_theme_support( 'post-thumbnails' );

	set_post_thumbnail_size( 689, 214, true );
    add_image_size( 'icon-cap-thesame', 40, 40, true );
    add_image_size( 'post-tiny-thumbnail-thesame', 50, 50, true );
    add_image_size( 'gallery-thumbnail-thesame', 276, 230, true );
    add_image_size( 'gallery-list-thumbnail-thesame', 446, 294, true );
    add_image_size( 'gallery-homepage-thumbnail-thesame', 197, 140, true );
    add_image_size( 'gallery-slide-thesame', 606, 480, true );
    add_image_size( 'slide-homepage-thumbnail-thesame', 940, 460, true );

    $pages = get_pages();

    create_page($pages, array('slug' => 'blog',	'title' =>'Blog'), 'template-blog.php');
    create_page($pages, array('slug' => 'gallery', 'title' =>'Gallery'), 'template-gallery.php');

    wp_insert_term(
        'Drop Cap',
        'category',
        array(
            'description'	=> 'Post shows on main page in first row',
            'slug' 		=> 'drop-cap'
        )
    );
    wp_insert_term(
        'Icon Cap',
        'category',
        array(
            'description'	=> 'Post shows on main page in second row',
            'slug' 		=> 'icon-cap'
        )
    );
}
endif; 
add_action( 'after_setup_theme', 'thesame_setup' );

// IE themify
function thesame_ie_support_header() {
    echo '<!--[if IE]>'. "\n";
    echo '<script src="' . esc_url( get_template_directory_uri() . '/js/html5.js' ) . '"></script>'. "\n";
    echo '<![endif]-->'. "\n";
}
add_action( 'wp_head', 'thesame_ie_support_header', 1 );


// Hook for changing default widget wrapper
function my_widget_content_wrap($content) {
    $content = '<p>'.$content.'</p>';
    return $content;
}
add_filter('widget_text', 'my_widget_content_wrap');


// Create Page with exists check
function create_page($pages, $args = null, $template = null) {
    $found = FALSE;
    foreach ($pages as $title) {
        switch ( $title->post_title ) {
            case 'Gallery' :
                $found = TRUE;
                break;
        }
    }
    if($found == FALSE) {
        $page_id = wp_insert_post(array(
            'post_title' => $args['title'],
            'post_type' =>'page',
            'post_name' => $args['slug'],
            'post_status' => 'publish',
        ));
        if (!empty($template)) add_post_meta( $page_id, '_wp_page_template', $template );
    }
}


// Configuring customizer options for media links located on-top of site header
function thesame_customizer( $wp_customize ) {

    // add "Content Options" section
    $wp_customize->add_section( 'header_links' , array(
        'title'      => __( 'Header Links', 'thesame' ),
        'priority'   => 100,
    ) );


    $wp_customize->add_setting(
        'linkedin_textbox',
        array(
            'default'     => 'http://'
        )
    );
    $wp_customize->add_control(
        'linkedin',
        array(
            'label'      => __( 'LinkedIn Link', 'thesame' ),
            'section' => 'header_links',
            'settings' => 'linkedin_textbox',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting(
        'facebook_textbox',
        array(
            'default'     => 'http://'
        )
    );
    $wp_customize->add_control(
        'facebook',
        array(
            'label'      => __( 'Facebook Link', 'thesame' ),
            'section' => 'header_links',
            'settings' => 'facebook_textbox',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting(
        'twitter_textbox',
        array(
            'default'     => 'http://'
        )
    );
    $wp_customize->add_control(
        'twitter',
        array(
            'label'      => __( 'Twitter Link', 'thesame' ),
            'section' => 'header_links',
            'settings' => 'twitter_textbox',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting(
        'rss_textbox',
        array(
            'default'     => 'http://'
        )
    );
    $wp_customize->add_control(
        'rss',
        array(
            'label'      => __( 'RSS Link', 'thesame' ),
            'section' => 'header_links',
            'settings' => 'rss_textbox',
            'type' => 'text',
        )
    );


    $wp_customize->add_section(
        'image_section' , array(
        'title'       => __( 'Theme images', 'thesame' ),
        'priority'    => 30,
        'description' => 'Site logo and default images selection',
    ) );

    $wp_customize->add_setting( 'logo' );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize,
        'logo_option', array(
            'label'    => __( 'Logo', 'thesame' ),
            'description' => 'Upload a logo to replace the default site name and description in the header',
            'section'  => 'image_section',
            'settings' => 'logo',
        )
    ));

    $wp_customize->add_setting( 'gallery-thumbnail-default' );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize,
        'default_image_option-1', array(
            'label'    => __( 'Default Gallery Thumbnail', 'thesame' ),
            'description' => 'Upload a default gallery thumbnail',
            'section'  => 'image_section',
            'settings' => 'gallery-thumbnail-default',
        )
    ));

    $wp_customize->add_setting( 'portfolio-homepage-thumbnail-default' );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize,
        'default_image_option-2', array(
            'label'    => __( 'Default Homepage Portfolio Thumbnail', 'thesame' ),
            'description' => 'Upload a default Homepage Portfolio thumbnail',
            'section'  => 'image_section',
            'settings' => 'portfolio-homepage-thumbnail-default',
        )
    ));

    $wp_customize->add_setting( 'portfolio-thumbnail-default' );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize,
        'default_image_option-3', array(
            'label'    => __( 'Default Portfolio Thumbnail', 'thesame' ),
            'description' => 'Upload a default Portfolio thumbnail',
            'section'  => 'image_section',
            'settings' => 'portfolio-thumbnail-default',
        )
    ));

    $wp_customize->add_setting( 'post-tiny-thumbnail-default' );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize,
        'default_image_option-4', array(
            'label'    => __( 'Default post-miniature Thumbnail', 'thesame' ),
            'description' => 'Upload a default post-miniature thumbnail',
            'section'  => 'image_section',
            'settings' => 'post-tiny-thumbnail-default',
        )
    ));

    $wp_customize->add_setting( 'icon-cap-default' );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize,
        'default_image_option-5', array(
            'label'    => __( 'Default Thumbnail for Icon-cap category posts', 'thesame' ),
            'description' => 'Upload a default Icon-cap category posts thumbnail',
            'section'  => 'image_section',
            'settings' => 'icon-cap-default',
        )
    ));


    $wp_customize->add_section(
        'copyright_section' , array(
        'title'       => __( 'Copyright', 'thesame' ),
        'priority'    => 30,
        'description' => __('Copyright text on site footer', 'thesame'),
    ) );

    $wp_customize->add_setting( 'copyright' );
    $wp_customize->add_control(
        'copyright_option',
        array(
            'label'      => __( 'Copyright text', 'thesame' ),
            'description' => __('Insert year - [year]<br>Insert site url - [link]', 'thesame'),
            'section' => 'copyright_section',
            'settings' => 'copyright',
            'type' => 'textarea',
        )
    );
}
add_action( 'customize_register', 'thesame_customizer' );

// Custom paginator for Galleries
function custom_pagination($numpages = '', $pagerange = '', $paged='') {

    if (empty($pagerange)) {
        $pagerange = 2;
    }

    /**
     * This first part of our function is a fallback
     * for custom pagination inside a regular loop that
     * uses the global $paged and global $wp_query variables.
     *
     * It's good because we can now override default pagination
     * in our theme, and use this function in default quries
     * and custom queries.
     */
    global $paged;
    if (empty($paged)) {
        $paged = 1;
    }
    if ($numpages == '') {
        global $wp_query;
        $numpages = $wp_query->max_num_pages;
        if(!$numpages) {
            $numpages = 1;
        }
    }

    /**
     * We construct the pagination arguments to enter into our paginate_links
     * function.
     */
    $pagination_args = array(
        'base'            => get_pagenum_link(1) . '%_%',
        'format'          => 'page/%#%',
        'total'           => $numpages,
        'current'         => $paged,
        'show_all'        => False,
        'end_size'        => 1,
        'mid_size'        => $pagerange,
        'prev_next'       => True,
        'prev_text'       => ('<span><</span>'),
        'next_text'       => ('<span>></span>'),
        'type'            => 'array',
        'add_args'        => false,
        'add_fragment'    => '',
        'before_page_number' => '<span>',
        'after_page_number' => '</span>',
    );

    $paginate_links = paginate_links($pagination_args);
    if( is_array( $paginate_links ) ) {
        echo '<ul class="pagenav">';
        foreach ( $paginate_links as $page ) {
            echo "<li>$page</li>";
        }
        echo '</ul>';
    }
}

add_filter( 'nav_menu_css_class', 'add_parent_url_menu_class', 10, 2 );

function add_parent_url_menu_class( $classes = array(), $item = false ) {
    // Get current URL
    $current_url = current_url();

    // Get homepage URL
    $homepage_url = trailingslashit( get_bloginfo( 'url' ) );

    // Exclude 404 and homepage
    if( is_404() or $item->url == $homepage_url ) return $classes;

    if ( strstr( $current_url, $item->url) ) {
        // Add the 'current-menu-item' class
        $classes[] = 'current-menu-item';
    }

    return $classes;
}

function current_url() {
    // Protocol
    $url = ( 'on' == $_SERVER['HTTPS'] ) ? 'https://' : 'http://';

    $url .= $_SERVER['SERVER_NAME'];

    // Port
    $url .= ( '80' == $_SERVER['SERVER_PORT'] ) ? '' : ':' . $_SERVER['SERVER_PORT'];

    $url .= $_SERVER['REQUEST_URI'];

    return trailingslashit( $url );
}

// Require a widget setup
require_once(get_template_directory() . '/shortcode.php');

// Require a widget setup
require_once(get_template_directory() . '/widget.php');
