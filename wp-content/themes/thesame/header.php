<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package The Same
 */
?>
<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_uri(); ?>" />
	<!--[if IE]>
	<script type="text/javascript" src="js/html5.js"></script>
	<![endif]-->
	<!--[if lte IE 7]>
	<link rel="stylesheet" type="text/css" href="css/ie7.css" />
	<![endif]-->
	<?php wp_head(); ?>
</head>
<body>
<!-- BEGIN STYLESHEET SWITCHER -->
<div id="stylesheet_switcher">
    <a href="#" id="switcher"></a>
    <ul id="stylesheets">
        <li>
            <a href="#" class="sheet" id="light">
                <span class="image"><img src="<?php echo get_template_directory_uri()?>/gfx/stylesheet_light.jpg" alt="" /></span>
                <span class="mask"></span>
                <span class="name"><?php _e('Light version', 'thesame'); ?></span>
            </a>
        </li>
        <li>
            <a href="#" class="sheet" id="dark">
                <span class="image"><img src="<?php echo get_template_directory_uri()?>/gfx/stylesheet_dark.jpg" alt="" /></span>
                <span class="mask"></span>
                <span class="name"><?php _e('Dark version', 'thesame'); ?></span>
            </a>
        </li>
    </ul>
</div>
<!-- END STYLESHEET SWITCHER -->
<!-- BEGIN PAGE -->
<div id="page">
	<div id="page_top">
		<div id="page_top_in">
			<!-- BEGIN TITLEBAR -->
			<header id="titlebar">
				<div class="wrapper">
                    <?php if ( get_theme_mod( 'logo' ) ) : ?>
                        <div class='site-logo'>
                            <a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img src='<?php echo esc_url( get_theme_mod( 'logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>
                        </div>
                    <?php else : ?>
                        <a id="logo" href="<?php echo get_home_url(); ?>"><span></span></a>
                    <?php endif; ?>
					<div id="titlebar_right">
						<ul id="social_icons">
                            <?php
                                $linkedin = get_theme_mod( 'linkedin_textbox', '' );
                                $facebook = get_theme_mod( 'facebook_textbox', '' );
                                $twitter = get_theme_mod( 'twitter_textbox', '' );
                                $rss = get_theme_mod( 'rss_textbox', '' );
                            ?>
							<?php if ($linkedin != '') : ?> <li><a href="<?php echo $linkedin ?>" class="linkedin"></a></li> <?php endif; ?>
                            <?php if ($facebook != '') : ?> <li><a href="<?php echo $facebook ?>" class="facebook"></a></li> <?php endif; ?>
                            <?php if ($twitter != '') : ?> <li><a href="<?php echo $twitter ?>" class="twitter"></a></li> <?php endif; ?>
                            <?php if ($rss != '') : ?> <li><a href="<?php echo $rss ?>" class="rss"></a></li> <?php endif; ?>
						</ul>
						<div class="clear"></div>
						<nav>
                            <?php
                                wp_nav_menu(array(
                                    'menu' => 'main',
                                    'items_wrap' => '<ul id="top_menu">%3$s</ul>',
                                    'walker' => new TheSame_Walker()
                                ));
                            ?>
						</nav>
					</div>
					<div class="clear"></div>
				</div>
			</header>
			<!-- END TITLEBAR -->