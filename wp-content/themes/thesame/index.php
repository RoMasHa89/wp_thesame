<?php
/**
 * The main template file.
 *
 * @package The Same
 */
get_header(); ?>
    <section id="content">
        <div class="wrapper page_text">
            <h1 class="page_title"><?php the_category(); ?></h1>
            <?php custom_breadcrumbs(); ?>
            <div class="columns">
                <div class="column column75">
                    <?php
                    if ( have_posts() ) { the_post(); ?>
                        <article class="article">
                            <div class="article_image nomargin">
                                <div class="inside">
                                    <?php the_post_thumbnail('blog-thumbnail-thesame') ?>
                                </div>
                            </div>
                            <div class="article_details">
                                <ul class="article_author_date">
                                    <li><em><?php _e('Add:', 'thesame') ?> </em><?php the_time( get_option( 'date_format' ) ); ?></li>
                                    <li><em><?php _e('Author:', 'thesame') ?> </em> <?php the_author_link();?></li>
                                </ul>
                                <p class="article_comments"><em><?php _e('Comment:', 'thesame') ?></em> <?php echo get_comments_number(); ?></p>
                            </div>
                            <h1><?php the_title(); ?></h1>
                            <?php if ($quote = get_field('quote')) : ?>
                                <q><?php echo $quote; ?></q>
                            <?php endif; ?>
                            <p><?php the_excerpt(); ?></p>
                            <div class="underline"></div>
                            <?php
                            // If comments are open or we have at least one comment, load up the comment template.
                            if ( comments_open() || get_comments_number() ) :
                                comments_template();
                            endif; ?>
                        </article>
                    <?php } else { get_template_part( 'content', 'none' ); } ?>
                </div>
                <?php  get_sidebar(); ?>
            </div>
    </section>
<?php get_footer(); ?>