<?php
/**
 * The template for displaying all pages.
 *
 * @package The Same
 */
get_header(); ?>
    <section id="content">
        <div class="wrapper page_text">
        <h1 class="page_title"><?php the_category(); ?></h1>
        <?php custom_breadcrumbs(); ?>
        <div class="columns">
            <div class="column column75">
<?php
    if ( have_posts() ) { the_post(); ?>
        <article class="article">
            <div class="article_image nomargin">
                <div class="inside">
                    <?php the_post_thumbnail('blog-thumbnail-thesame') ?>
                </div>
            </div>
            <div class="article_details">
                <ul class="article_author_date">
                    <li><em><?php _e('Add:', 'thesame') ?> </em><?php the_time( get_option( 'date_format' ) ); ?></li>
                    <li><em><?php _e('Author:', 'thesame') ?> </em> <?php the_author_link();?></li>
                </ul>
                <?php if ( comments_open() || get_comments_number() ) : ?>
                    <p class="article_comments"><em><?php _e('Comment:', 'thesame') ?></em> <?php echo get_comments_number(); ?></p>
                <?php endif; ?>
            </div>
            <h1><?php the_title(); ?></h1>
            <?php if ($quote = get_field('quote')) : ?>
                <q><?php echo $quote; ?></q>
            <?php endif; ?>
            <p><?php the_content(); ?></p>
            <div class="underline"></div>
            <?php
            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif;
            ?>
        </article>
        <?php
        $prev_link = get_previous_posts_link(__('Next posts', 'thesame'));
        $next_link = get_next_posts_link(__('Previous Posts', 'thesame'));
        // as suggested in comments
        if ($prev_link || $next_link) { ?>
            <nav class="pagination">
                <span class="pagi-prev"><?php echo $prev_link; ?></span>
                <span class="pagi-next"><?php echo $next_link; ?></span>
            </nav>
        <?php } ?>
    <?php } else { get_template_part( 'content', 'none' ); } ?>
            </div>
        </div>
    </section>
<?php get_footer(); ?>