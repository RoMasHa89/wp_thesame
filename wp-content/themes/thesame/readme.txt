/*
Theme Name: The Same
Author: Alex Romantsov
Theme URI: --
Author URI: --
Description: The Same is an attractive free WordPress theme available business websites or blogs. This free wordpress themes also supports HTML5/CSS3 and responsive layout. Set the branding with ease with our user friendly and detailed admin options.
Version: 1.0.0
Tags: green, black, white, light, one-column, two-columns, left-sidebar, responsive-layout, custom-menu, custom-background, editor-style, featured-images, full-width-template, theme-options, threaded-comments, translation-ready
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Text Domain: thesame
*/

== Copyright ==
The Same WordPress Theme, Copyright (C) 2015
The Same WordPress Theme is licensed under the GPL 3.

The Same is built with the following resources:

Options Framework Theme - -
License: GNU General Public License
Copyright: wptheming, http://wptheming.com

Main JS  - http://www.pwtthemes.com/
License: General Public License (GPL)
Copyright: PWT, http://www.pwtthemes.com/

== Installation ==

1. Upload the `The Same` folder to the `/wp-content/themes/` directory
Activation and Use
1. Activate the Theme through the 'Themes' menu in WordPress
2. See Appearance -> Theme Options to change theme options
