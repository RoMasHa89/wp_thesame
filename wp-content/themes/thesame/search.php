<?php
/**
 * The template for displaying search
 *
 * @package The Same
 */
get_header(); ?>
<section id="content">
    <div class="wrapper page_text">
        <h1><?php printf( __( 'Search Results for: %s', 'thesame' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
        <div class="underline"></div>
        <div class="columns">
            <div class="column column75">
                <?php if ( have_posts() ) { the_post(); ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <?php get_template_part( 'content', 'posts');  ?>
                    <?php endwhile; ?>
                <?php } else { get_template_part( 'content', 'none' ); } ?>
                <div class="underline"></div>
                <?php
                $prev_link = get_previous_posts_link(__('Next results', 'thesame'));
                $next_link = get_next_posts_link(__('Previous results', 'thesame'));
                // as suggested in comments
                if ($prev_link || $next_link) { ?>
                    <nav class="pagination">
                        <span class="pagi-prev"><?php echo $prev_link; ?></span>
                        <span class="pagi-next"><?php echo $next_link; ?></span>
                    </nav>
                <?php } ?>
            </div>
            <?php  get_sidebar(); ?>
        </div>
</section>
<?php get_footer(); ?>

