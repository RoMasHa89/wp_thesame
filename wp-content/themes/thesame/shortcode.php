<?php

// Add a shortcode for gallery output
function gallery_shortcode_func( $atts ) {
    // Getting arguments
    extract( $atts );

    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    if (empty($count)) {
        $count = 4;
    }
    // Getting data of posts with cat "portfolio" from database
    $args = array(
        'post_type' => 'portfolio',
        'posts_per_page' => $count,
        'paged' => $paged,
    );
    $loop = new WP_Query( $args );
    // if count of posts is greater then 0, starting to print posts content on page
    if ($loop->post_count) { ?>
        <div class="page_gallery">
        <div class="columns">
            <?php
            $n=0;
            while ($loop->have_posts()) :
                // Goto next post
                $loop->the_post();
                if (empty($image = get_field("portfolio-thumbnail"))) {
                    $thumb = esc_url( get_theme_mod( 'gallery-thumbnail-default', get_template_directory_uri() . "/gfx/gallery-thumbnail-default.jpg" ) );
                } else {
                    $thumb = $image['sizes']['gallery-list-thumbnail-thesame'];
                }
                $images_full = explode(',', types_render_field("gallery-image", array(
                    'raw' => 'true',
                    'separator'=>','
                )));
                $overview = strip_tags(get_field("overview"));
                $overview = strlen($overview) < 50 ? $overview : mb_substr($overview,0,50) . '...';
                $n++;
                if ($n > 1 && $n & 1) : echo '</div><div class="columns">'; endif;  ?>
                <div class="column column50">
                    <div class="image">
                        <img src="<?php echo $thumb; ?>" alt="" />
                        <p class="caption">
                            <strong><?php echo get_field("photo_title"); ?></strong>
                            <span><?php echo $overview; ?></span>
                            <?php foreach ($images_full as $url) { if ($url != '') : ?>
                                <a href="<?php echo $url; ?>" data-rel="prettyPhoto[<?php echo get_the_ID();?>]" class="button button_small button_orange float_right lightbox"><span class="inside">zoom</span></a>
                            <?php endif; } ?>
                        </p>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    <?php }
    wp_reset_postdata();
    if (function_exists(custom_pagination)) {
        custom_pagination($loop->max_num_pages,"",$paged);
    }
}
add_shortcode( 'gallery', 'gallery_shortcode_func' );


// Add a shortcode for Slider output
function slider_shortcode_func( $atts ) {
    // Getting arguments
    extract( $atts );

    if (!isset($count) || empty($count)) {
        $count = 10;
    }

    // Getting data of posts from database
    $args = array(
        'post_type' => 'portfolio',
        'posts_per_page' => $count,
    );
    $loop = new WP_Query( $args );
    // if count of posts is greater then 0, starting to print posts content on page
    if ($loop->post_count) { ?>
        <!-- BEGIN TOP -->
        <section id="top">
            <div class="wrapper">
                <div id="top_slide" class="flexslider">
                    <ul class="slides">
                    <?php while ($loop->have_posts()) : $loop->the_post(); ?>
        <?php
        if (!empty($image = get_field("portfolio-thumbnail"))) :
            $thumb = $image['sizes']['slide-homepage-thumbnail-thesame']; ?>
            <li>
                <img src="<?php echo $thumb; ?>">
                <p class="flex-caption">
                    <strong><?php echo get_the_title(); ?></strong>
                    <span><?php echo get_the_excerpt(); ?></span>
                </p>
            </li>
        <?php endif; ?>
    <?php endwhile; ?>
                    </ul>
                </div>
            </div>
        </section>
        <!-- END TOP -->
    <?php } wp_reset_query();
}
add_shortcode( 'slider', 'slider_shortcode_func' );



// Add a shortcode for Home Page content output
function homepage_content_shortcode_func() {
?>
    <section id="content">
        <div class="wrapper page_text page_home">
            <?php if ( is_active_sidebar('main-page-intro-area') ) : ?>
                <?php dynamic_sidebar('main-page-intro-area'); ?>
            <?php endif;

            // Getting data of posts with cat "drop-cap" with shortcode
            echo do_shortcode("[drop-cap-posts]");

            // Getting data of posts with cat "icon-cap" with shortcode
            echo do_shortcode("[icon-cap-posts]");

            if ( is_active_sidebar('bottom-widget-area') ) : ?>
                <div class="underline"></div>
                <?php dynamic_sidebar('bottom-widget-area'); ?>
            <?php endif; ?>
        </div>
    </section> <?php
}
add_shortcode( 'homepage_content', 'homepage_content_shortcode_func' );



// Add a shortcode for Icon Cap content output
function icon_cap_shortcode_func() {
    // Getting data of posts with cat "icon-cap" from database
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => 3,
        'category_name'    => 'icon-cap',
    );
    $loop = new WP_Query( $args );
    // if count of posts is greater then 0, starting to print posts content on page
    if ($loop->post_count) { ?>
        <ul class="columns iconcap">
            <?php while ($loop->have_posts()) :
                // Goto next post
                $loop->the_post();
                if (empty($img = get_field("icon"))) {
                    $img = esc_url( get_theme_mod( 'icon-cap-default', get_template_directory_uri() . "/gfx/icon-cap-default.png" ) );
                } ?>
                <li class="column column33">
                    <img class="iconcap-img" src="<?php echo $img; ?>" style="float: left; max-width: 40px; max-height: 40px;">
                    <div class="inside">
                        <h1><?php echo the_title(); ?></h1>
                        <p><?php the_excerpt(); ?></p>
                        <p class="read_more"><a href="<?php echo get_permalink(get_the_ID()); ?>"><?php _e('Read More', 'thesame'); ?></a></p>
                    </div>
                </li>
            <?php endwhile; ?>
        </ul >
    <?php }
    wp_reset_query();
}
add_shortcode( 'icon-cap-posts', 'icon_cap_shortcode_func' );


// Add a shortcode for Drop Cap content output
function drop_cap_shortcode_func() {
    // Getting data of posts with cat "drop-cap" from database
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => 3,
        'category_name'    => 'drop-cap',
    );
    $loop = new WP_Query( $args );
    // if count of posts is greater then 0, starting to print posts content on page
    if ($loop->post_count) { ?>
                <ul class="columns dropcap">
            <?php
        $n = 0;
        $pos = array('first', 'second', 'third');
        while ($loop->have_posts()) :
            // Goto next post
            $loop->the_post();
            $number = $pos[$n];
            $n++;
            ?>
            <li class="column column33 <?php echo $number; ?>">
                <div class="inside">
                    <h1><?php echo the_title(); ?></h1>
                    <p><?php echo the_excerpt(); ?></p>
                    <p class="read_more"><a href="<?php echo get_permalink(get_the_ID()); ?>"><?php _e('Read More', 'thesame'); ?></a></p>
                </div>
            </li>
        <?php endwhile; ?>
                </ul >
            <?php }
    wp_reset_query();
}
add_shortcode( 'drop-cap-posts', 'drop_cap_shortcode_func' );


