<?php
/**
 * The Sidebar containing a "Blog" widget area.
 *
 * @package The Same
 */
?>
<div class="column column25">
    <?php if ( is_active_sidebar('blog-sidebar-widget-area') ) : ?>
        <?php dynamic_sidebar('blog-sidebar-widget-area'); ?>
    <?php endif; ?>
</div>