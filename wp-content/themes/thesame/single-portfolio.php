<?php
/**
 * The template for displaying category
 *
 * Template Name: Portfolio
 *
 */
get_header();
if ( have_posts() ) { the_post(); ?>
		<section id="content">
			<div class="wrapper page_text">
				<h1 class="page_title"><?php if(get_the_title($post->ID)) { the_title(); } else { the_time( get_option( 'date_format' ) ); } ?></h1>
                <?php custom_breadcrumbs(); ?>
				<div class="columns">
					<div class="column column33">
						<h1><?php echo get_field("photo_title"); ?></h1>
						<p><?php echo get_field("overview"); ?></p>
						<h1><?php _e('Client:', 'thesame'); ?></h1>
						<p><?php echo get_field("client"); ?></p>
						<h1><?php _e('Model & Photographer:', 'thesame'); ?></h1>
						<p><?php echo get_field("model-photographer"); ?></p>
					</div>
                    <?php
                        $images_thumbs = explode(',', types_render_field("gallery-image", array(
                            'size'=>'gallery-slide-thesame',
                            'url' => 'true',
                            'separator'=>','
                        )));
                        $images_full = explode(',', types_render_field("gallery-image", array(
                                'raw' => 'true',
                                'separator'=>','
                        )));
                    ?>
					<div class="column column66">
						<div id="content_slide">
							<div class="flexslider">
								<ul class="slides">
                                    <?php foreach ($images_full as $key => $url) { ?>
                                        <li><a href="<?php echo $url; ?>" class="lightbox" data-rel="prettyPhoto[gallery]"><img src="<?php echo $images_thumbs[$key]; ?>" alt="<?php echo $key+1 ?>" /></a></li>
                                    <?php } ?>
                                </ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
        <div class="underline"></div>
<?php } else { ?>
        <section id="content">
            <div class="wrapper page_text">
                <h1><?php _e( 'There is no content yet!', 'thesame' ); ?></h1>
                <div class="underline"></div>
        </section>
<?php } get_footer(); ?>