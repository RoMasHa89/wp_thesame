<?php
/**
 * The template for displaying category
 *
 * Template Name: Portfolio
 *
 */
get_header();
if ( have_posts() ) : ?>
    <section id="content">
        <div class="wrapper page_text page_home">
<?php
        custom_breadcrumbs();
        $parts = explode('/', rtrim($_SERVER['REQUEST_URI'],'/'));
        $selected_cat = end($parts);
        // Getting data of posts with queried cat from database
        $args = array(
            'post_type' => 'portfolio',
            'portfolio-category' => $selected_cat
        );
        $loop = new WP_Query( $args );
        // if count of posts is greater then 0, starting to print posts content on page
        if ($loop->post_count) { ?>
            <div class="portfolio_items_container">
                <ul class="portfolio_items columns">
                    <?php
                    $n = 0;
                    while ($loop->have_posts()) :
                        // Goto next post
                        $loop->the_post();
                        if (empty($image = get_field("portfolio-thumbnail"))) {
                            $thumb = $img = esc_url( get_theme_mod( 'portfolio-thumbnail-default', get_template_directory_uri() . "/gfx/portfolio-thumbnail-default.jpg" ) );
                        } else {
                            $thumb = $image['sizes']['gallery-thumbnail-thesame'];
                            $img = $image['url'];
                        }
                        $slug = wp_get_post_terms(get_the_ID(), 'portfolio-category', array("fields" => "all"));
                        $n++;
                    ?>
                        <li data-type="<?php echo $slug[0]->slug; ?>" data-id="id-<?php echo $n; ?>" class="column column33">
                            <a href="<?php echo $img; ?>" data-rel="prettyPhoto[gallery]" class="portfolio_image lightbox">
                                <div class="inside">
                                    <img alt="" src="<?php echo $thumb; ?>">
                                    <div class="mask"></div>
                                </div>
                            </a>
                            <h1><?php echo the_title(); ?></h1>
                            <p><?php echo the_excerpt(); ?></p>
                            <a class="button button_small button_orange" href="<?php echo get_permalink(get_the_ID()); ?>"><span class="inside"><?php _e('Read More', 'thesame'); ?></span></a>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </div>
        <?php } wp_reset_query(); ?>
        </div>
    </section>
<?php else : ?>
    <section id="content">
        <div class="wrapper page_text">
            <h1><?php _e( 'There is no content yet!', 'thesame' ); ?></h1>
            <div class="underline"></div>
    </section>
    <?php endif;
get_footer(); ?>