<?php
/**
 * The template for displaying Page - Gallery
 *
 * Template Name: Blog
 *
 */
get_header();
if ( have_posts() ) { ?>
    <section id="content">
        <div class="wrapper page_text">
        <h1 class="page_title"><?php _e('Blog', 'thesame'); ?></h1>
        <?php custom_breadcrumbs(); ?>
        <div class="columns">
<?php
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    // Getting data of posts with cat "portfolio" from database
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => get_option('posts_per_page'),
        'paged' => $paged,
    );
    $loop = new WP_Query( $args );
    // if count of posts is greater then 0, starting to print posts content on page
    if ($loop->post_count) { ?>
        <div class="column column75">
        <?php while ($loop->have_posts()) : $loop->the_post(); ?>
            <article class="article">
                <div class="article_image nomargin">
                    <div class="inside">
                        <?php the_post_thumbnail() ?>
                    </div>
                </div>
                <div class="article_details">
                    <ul class="article_author_date">
                        <li><em><?php _e('Add:', 'thesame') ?> </em><?php the_time( get_option( 'date_format' ) ); ?></li>
                        <li><em><?php _e('Author:', 'thesame') ?> </em> <?php the_author_link();?></li>
                    </ul>
                    <p class="article_comments"><em><?php _e('Comment:', 'thesame') ?></em> <?php echo get_comments_number(); ?></p>
                </div>
                <h1><?php the_title(); ?></h1>
                <?php if ($quote = get_field('quote')) : ?>
                <q><?php echo $quote; ?></q>
                <?php endif; ?>
                <p><?php the_excerpt(); ?></p>
                <a class="button button_small button_orange float_left" href="<?php echo get_permalink(get_the_ID()); ?>"><span class="inside"><?php _e('Read More', 'thesame'); ?></span></a>
            </article>
        <?php endwhile;
        if (function_exists(custom_pagination)) {
            custom_pagination($loop->max_num_pages,"",$paged);
        } ?>
        </div>
    <?php } wp_reset_query(); ?>

    <?php get_sidebar(); ?>
				</div>
			</div>
		</section>
<?php } else { ?>
        <section id="content">
            <div class="wrapper page_text">
                <h1><?php _e( 'There is no content yet!', 'thesame' ); ?></h1>
                <div class="underline"></div>
        </section>
<?php }
get_footer(); ?>