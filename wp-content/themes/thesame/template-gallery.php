<?php
/**
 * The template for displaying Page - Gallery
 *
 * Template Name: Gallery
 *
 */
get_header();
if ( have_posts() ) : the_post(); ?>
    <section id="content">
        <div class="wrapper page_text page_home">
        <h1 class="page_title"><?php _e('Gallery', 'thesame'); ?></h1>
<?php
    custom_breadcrumbs();
    the_content();
?>
        </div>
    </section>

<?php else : ?>
    <section id="content">
        <div class="wrapper page_text">
            <h1><?php _e( 'There is no content yet!', 'thesame' ); ?></h1>
            <div class="underline"></div>
    </section>
<?php endif;
get_footer(); ?>