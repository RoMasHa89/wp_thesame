<?php

if (!function_exists('thesame_widgets_init')) :
    function thesame_widgets_init()
    {
        register_sidebar(array(
            'name' => __('Main Page Introduction Area', "thesame"),
            'id' => 'main-page-intro-area',
            'description' => __('Main page introduction post area', "thesame"),
            'before_widget' => '<div id="%1$s" class="introduction %2$s"> ',
            'after_widget' => '</div>',
            'before_title' => '<h1>',
            'after_title' => '</h1>',
        ));
        register_sidebar(array(
            'name' => __('Blog Sidebar Widget Area', "thesame"),
            'id' => 'blog-sidebar-widget-area',
            'description' => __('The sidebar widget area on Blog page', "thesame"),
            'before_widget' => '<div id="%1$s" class="padd16bot %2$s"> ',
            'after_widget' => '</div>',
            'before_title' => '<h1>',
            'after_title' => '</h1>',
        ));
        register_sidebar(array(
            'name' => __('Bottom Widget Area', "thesame"),
            'id' => 'bottom-widget-area',
            'description' => __('Bottom widget area', "thesame"),
            'before_widget' => '<div id="%1$s" class="portfolio %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h1>',
            'after_title' => '</h1>'
        ));

        register_sidebar(array(
            'name' => __('Footer Widget Area', "thesame"),
            'id' => 'footer-widget-area',
            'description' => __('The footer widget area', "thesame"),
            'before_widget' => '<div id="%1$s" class="box %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h3>',
            'after_title' => '</h3>'
        ));

        register_widget('Intro_Widget');
        register_widget('Custom_Search_Widget');
        register_widget('Mod_Widget_Recent_Posts');
        register_widget('Contact_Us_Widget');
        register_widget('list_categories_widget');
        register_widget('list_terms_widget');
        register_widget('portfolio_widget');


    }
endif;
add_action('widgets_init', 'thesame_widgets_init');


// Creating new widget "Inrtoduction" for HomePage
class Intro_Widget extends WP_Widget
{

    // Initialization parameters of new widget
    public function __construct()
    {
        parent::__construct("intro-widget", __("[The Same] Main Page Intro Widget", 'thesame'),
            array("description" => _("Widget, that shows specified content only on main page in top side of page")));
    }

    // Function that constructs widget on site page
    function widget($args, $instance)
    {
        // Getting arguments
        extract($args);
        echo $before_widget;
        // Display the widget title
        echo $before_title . $instance['title'] . $after_title;
        ?>
        <p><?php echo $instance['teaser'] ?></p>
        <a class="button button_big button_orange float_left" href="<?php echo $instance['link']; ?>">
            <span class="inside"><?php _e('Read More', 'thesame'); ?></span>
        </a>
        <?php
        // Loop of parsing 'thesame' posts, while it's end

        echo $after_widget;
    }

    //Update the widget settings
    function update($new_instance, $old_instance)
    {
        $values = array();

        //Strip tags from title to remove HTML. Save new settings
        $values['title'] = $new_instance['title'];
        $values['teaser'] = $new_instance['teaser'];
        $values['link'] = $new_instance['link'];

        return $values;
    }


    function form($instance)
    {

        //Set up some default widget settings.
        $defaults = array('title' => __('Introduction', 'thesame'), 'teaser' => '', 'link' => 'http://');
        $instance = wp_parse_args((array)$instance, $defaults);

        //Get widget fields values
        if (!empty($instance)) {
            $title = $instance['title'];
            $teaser = $instance['teaser'];
            $link = $instance['link'];
        }

        // Get widget field ID and name
        $tableId = $this->get_field_id('title');
        $tableName = $this->get_field_name('title');
        ?>
        <!-- html layout widget -->
        <br><label for="<?php echo $tableId ?>"><?php echo __('Link with selected post:', 'thesame') ?></label><br>
        <select name="<?php echo $tableName ?>" id="<?php echo $tableId; ?>" class="widefat">
        <?php
            // Getting data of posts with cat "icon-cap" from database
            $args = array(
                'post_type' => 'post',
            );
            $loop = new WP_Query( $args );
            // if count of posts is greater then 0, starting to print posts content on page
            if ($loop->post_count) {
                while ($loop->have_posts()) :
                    // Goto next post
                    $loop->the_post();
                    if ($title == get_the_title()) {
                        $select = ' selected="selected"';
                        if ($teaser == '') {
                            $teaser = get_the_excerpt();
                        }
                        $link = get_permalink(get_the_ID());
                    } else {
                        $select = '';
                    }
                    echo '<option id="' . get_the_ID () . '"', $select, '>', get_the_title(), '</option>';
                endwhile;
            }
            wp_reset_query(); ?>
		</select><br>
        <?php
        // Get widget field ID and name
        $tableId = $this->get_field_id('teaser');
        $tableName = $this->get_field_name('teaser');
        ?>
        <br><label for="<?php echo $tableId ?>"><?php echo __('Teaser:', 'thesame') ?></label><br>
        <textarea name="<?php echo $tableName ?>" id="<?php echo $tableId ?>" wrap="soft" rows="10" class="widefat"><?php echo $teaser ?></textarea>
        <i>*leave empty to update content from selected post. Will be updated after click on "Save" button</i><br><br> <?php
        // Get widget field ID and name
        $tableId = $this->get_field_id('link');
        $tableName = $this->get_field_name('link');
        ?>
        <!-- html layout widget -->
        <label for="<?php echo $tableId ?>"><?php echo __('Link:', 'thesame') ?></label><br>
        <input id="<?php echo $tableId ?>" type="text" name="<?php echo $tableName ?>" value="<?php echo $link ?>" readonly class="widefat"><br><br><?php
    }
}

// Creating new widget "Custom Search"
class Custom_Search_Widget extends WP_Widget
{

    // Initialization parameters of new widget
    public function __construct()
    {
        parent::__construct("custom-search-widget", __("[The Same] Search Widget", 'thesame'),
            array("description" => _("Specially stylized widget for searching on site")));
    }

    // Function that constructs widget on site page
    function widget($args, $instance)
    {
        // Getting arguments
        extract($args);
        echo $before_widget;
        // Display the widget title
        echo $before_title . $instance['title'] . $after_title;
        ?>
        <form role="search" method="get" id="searchform" class="searchform searchbar" action="">
            <fieldset>
                <div>
                    <span class="input_text"><input name="s" id="s" type="text" class="clearinput" value="<?php _e('Search...', 'thesame'); ?>" /></span>
                    <input id="searchsubmit" value="" type="submit" class="input_submit">
                </div>
            </fieldset>
        </form>
        <?php
        // Loop of parsing 'thesame' posts, while it's end
        echo $after_widget;
    }

    //Update the widget settings
    function update($new_instance, $old_instance)
    {
        $values = array();

        //Strip tags from title to remove HTML. Save new settings
        $values['title'] = $new_instance['title'];

        return $values;
    }


    function form($instance)
    {

        //Set up some default widget settings.
        $defaults = array('title' => __('Search', 'thesame'));
        $instance = wp_parse_args((array)$instance, $defaults);

        //Get widget fields values
        if (!empty($instance)) {
            $title = $instance['title'];
        }

        // Get widget field ID and name
        $tableId = $this->get_field_id('title');
        $tableName = $this->get_field_name('title');
        ?>
        <!-- html layout widget -->
        <label for="<?php echo $tableId ?>"><?php echo __('Title:', 'thesame') ?></label><br>
        <input id="<?php echo $tableId ?>" type="text" name="<?php echo $tableName ?>" value="<?php echo $title ?>"><br>
    <?php

    }
}

/**
 * List Categories Widget Class
 */
class list_categories_widget extends WP_Widget {


    /** constructor -- name this the same as the class above */
    function list_categories_widget() {
        parent::WP_Widget(false, $name = __('[The Same] List Categories', 'thesame'));
    }

    /** @see WP_Widget::widget -- do not rename this */
    function widget($args, $instance) {
        extract( $args );
        $title 		= apply_filters('widget_title', $instance['title']); // the widget title
        $number 	= $instance['number']; // the number of categories to show
        $taxonomy 	= $instance['taxonomy']; // the taxonomy to display

        ?>
        <?php echo $before_widget; ?>
        <?php if ( $title ) { echo $before_title . $title . $after_title; } ?>
        <ul class="menu categories page_text">
        <?php foreach ($taxonomy as $tax) {
                $args = array(
                    'number' 	=> $number,
                    'taxonomy'	=> $tax
                );
                // retrieves an array of categories or taxonomy terms
                $tax_object = get_taxonomy( $tax );
                $cats = get_categories($args);
                $link = get_post_type_archive_link($tax_object->object_type[0]);
                if (empty($link)) : $link = get_home_url(); endif;
                if (!empty($cats)) { ?>
                    <li>
                        <a href="<?php echo $link; ?>"><?php echo $tax_object->label; ?></a>
                        <ul>
                        <?php
                        foreach ($cats as $cat) { ?>
                            <li>
                                <a href="<?php echo get_term_link($cat->slug, $tax); ?>" title="<?php sprintf(__("View all posts in %s", 'thesame'), $cat->name); ?>"><?php echo $cat->name; ?> (<?php echo $cat->count; ?>)</a>
                            </li>
                        <?php
                        } ?>
                        </ul>
                    </li> <?php
                }
            } ?>
        </ul>
        <?php echo $after_widget;
    }

    /** @see WP_Widget::update -- do not rename this */
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['number'] = strip_tags($new_instance['number']);
        $instance['taxonomy'] = $new_instance['taxonomy'];
        return $instance;
    }

    /** @see WP_Widget::form -- do not rename this */
    function form($instance) {

        //Set up some default widget settings.
        $defaults = array('title' => __('Categories', 'thesame'), 'number' => 15, 'taxonomy' => '');
        $instance = wp_parse_args((array)$instance, $defaults);

        //Get widget fields values
        if (!empty($instance)) {
            $title 		= esc_attr($instance['title']);
            $number		= esc_attr($instance['number']);
            $taxonomy	= $instance['taxonomy'];
        }

        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'thesame'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of categories to display'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('taxonomy'); ?>"><?php _e('Choose the Taxonomies to display', 'thesame'); ?></label><br>
            <?php
            $taxonomies = get_taxonomies(array('public'=>true), 'names');
            foreach ($taxonomies as $id => $name) {
                ?>
                    <input id="<?php echo $this->get_field_id('taxonomy') . $id; ?>" name="<?php echo $this->get_field_name('taxonomy'); ?>[]" type="checkbox" value="<?php echo $name; ?>" <?php checked('1', in_array($name,$taxonomy)); ?> /> <?php echo $name; ?><br>
                <?php
            }
            ?>
        </p>
    <?php
    }
} // end class list_categories_widget


/**
 * List Terms Widget Class
 */
class list_terms_widget extends WP_Widget {


    /** constructor -- name this the same as the class above */
    function list_terms_widget() {
        parent::WP_Widget(false, $name = __('[The Same] Terms Categories', 'thesame'));
    }

    /** @see WP_Widget::widget -- do not rename this */
    function widget($args, $instance) {
        extract( $args );
        $title 		= apply_filters('widget_title', $instance['title']); // the widget title
        $number 	= $instance['number']; // the number of categories to show
        $taxonomy 	= $instance['taxonomy']; // the taxonomy to display

        ?>
        <?php echo $before_widget; ?>
        <?php if ( $title ) { echo $before_title . $title . $after_title; } ?>
        <ul class="menu categories page_text">
            <?php foreach ($taxonomy as $tax) {
                $args = array(
                    'number' 	=> $number+1,
                    'taxonomy'	=> $tax
                );
                // retrieves an array of categories or taxonomy terms
                $tax_object = get_taxonomy( $tax );
                $terms = get_categories($args);
                $link = get_post_type_archive_link($tax_object->object_type[0]);
                if (empty($link)) : $link = get_home_url(); endif;
                if (!empty($terms)) { ?>
                    <?php
                    foreach ($terms as $term) {
                        if ($term->parent == 0) {
                            $children = get_term_children( $term->term_id, $tax ); ?>
                            <li>
                                <a href="<?php echo get_term_link($term->slug, $tax); ?>" title="<?php sprintf(__("View all posts in %s", 'thesame'), $term->name); ?>">
                                    <?php echo $term->name; ?> (<?php echo empty($children) ? $term->count : count($children); ?>)
                                </a> <?php
                                if ( !empty($children) ) { ?>
                                    <ul>
                                        <li>
                                            <?php foreach ( $children as $child ) {
                                                $term = get_term_by( 'id', $child, $tax ); ?>
                                                <a href="<?php echo get_term_link( $child, $tax ); ?>" >
                                                    <?php echo $term->name; ?>  (<?php echo $term->count; ?>)
                                                </a>
                                            <?php } ?>
                                        </li>
                                    </ul> <?php
                                } ?>
                            </li> <?php
                        }
                    }
                }
            } ?>
        </ul>
        <?php echo $after_widget;
    }

    /** @see WP_Widget::update -- do not rename this */
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['number'] = strip_tags($new_instance['number']);
        $instance['taxonomy'] = $new_instance['taxonomy'];
        return $instance;
    }

    /** @see WP_Widget::form -- do not rename this */
    function form($instance) {

        //Set up some default widget settings.
        $defaults = array('title' => __('Categories', 'thesame'), 'number' => 15, 'taxonomy' => '');
        $instance = wp_parse_args((array)$instance, $defaults);

        //Get widget fields values
        if (!empty($instance)) {
            $title 		= esc_attr($instance['title']);
            $number		= esc_attr($instance['number']);
            $taxonomy	= $instance['taxonomy'];
        }

        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'thesame'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of terms to display'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('taxonomy'); ?>"><?php _e('Choose the Taxonomies to display', 'thesame'); ?></label><br>
            <?php
            $taxonomies = get_taxonomies(array('public'=>true), 'names');
            foreach ($taxonomies as $id => $name) {
                ?>
                <input id="<?php echo $this->get_field_id('taxonomy') . $id; ?>" name="<?php echo $this->get_field_name('taxonomy'); ?>[]" type="checkbox" value="<?php echo $name; ?>" <?php checked('1', in_array($name,$taxonomy)); ?> /> <?php echo $name; ?><br>
            <?php
            }
            ?>
        </p>
    <?php
    }
} // end class list_terms_widget


/**
 * Recent_Posts widget class
 *
 * @since 2.8.0
 */
class Mod_Widget_Recent_Posts extends WP_Widget {

    function __construct() {
        $widget_ops = array('classname' => 'widget_recent_entries', 'description' => __( "The most recent posts on your site", 'thesame') );
        parent::__construct('mod-recent-posts', __('[The same] Recent Posts', 'thesame'), $widget_ops);
        $this->alt_option_name = 'widget_recent_entries';

        add_action( 'save_post', array($this, 'flush_widget_cache') );
        add_action( 'deleted_post', array($this, 'flush_widget_cache') );
        add_action( 'switch_theme', array($this, 'flush_widget_cache') );
    }

    function widget($args, $instance) {
        $cache = wp_cache_get('widget_recent_posts', 'widget');

        if ( !is_array($cache) )
            $cache = array();

        if ( ! isset( $args['widget_id'] ) )
            $args['widget_id'] = $this->id;

        if ( isset( $cache[ $args['widget_id'] ] ) ) {
            echo $cache[ $args['widget_id'] ];
            return;
        }

        ob_start();
        extract($args);

        $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Posts' );
        $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
        $number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 10;
        if ( ! $number )
            $number = 10;
        $show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

        $r = new WP_Query( apply_filters( 'widget_posts_args', array( 'posts_per_page' => $number, 'no_found_rows' => true, 'post_status' => 'publish', 'ignore_sticky_posts' => true, 'post_type' => array('post', 'portfolio', 'blog') ) ) );
        if ($r->have_posts()) :
            ?>
            <?php echo $before_widget; ?>
            <?php if ( $title ) echo $before_title . $title . $after_title; ?>
            <ul class="recent_posts">
                <?php while ( $r->have_posts() ) : $r->the_post();
                    if (!$content = strip_tags(get_the_content())) {
                        $content = 'Empty content...';
                    } else {
                        $content = strlen($content) <= 40 ? $content : mb_substr($content,0,40).'...';
                    }
                    $img_title = get_the_title() ? get_the_title() : get_the_ID();
                    $img = has_post_thumbnail(get_the_ID()) ? get_the_post_thumbnail( get_the_ID(), 'post-tiny-thumbnail-thesame') :
                        "<img alt='$img_title' src='" . esc_url( get_theme_mod( 'post-tiny-thumbnail-default', get_template_directory_uri() . "/gfx/post-tiny-thumbnail-default.png" ) ) . "' >";

                    ?>
                    <li class="item">
                        <a class="thumbnail" href="<?php the_permalink() ?>"><?php echo $img; ?></a>
                        <div class="text">
                            <h4 class="title"><a href="<?php the_permalink() ?>"><?php echo $content; ?></a></h4>
                            <?php if ( $show_date ) : ?>
                            <p class="data">
                                <span class="date"><?php the_time( get_option( 'date_format' ) ); ?></span>
                            </p>
                            <?php endif; ?>
                        </div>
                    </li>
                <?php endwhile; ?>
            </ul>
            <?php echo $after_widget; ?>
            <?php
            // Reset the global $the_post as this query will have stomped on it
            wp_reset_postdata();

        endif;

        $cache[$args['widget_id']] = ob_get_flush();
        wp_cache_set('widget_recent_posts', $cache, 'widget');
    }

    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['number'] = (int) $new_instance['number'];
        $instance['show_date'] = (bool) $new_instance['show_date'];
        $this->flush_widget_cache();

        $alloptions = wp_cache_get( 'alloptions', 'options' );
        if ( isset($alloptions['widget_recent_entries']) )
            delete_option('widget_recent_entries');

        return $instance;
    }

    function flush_widget_cache() {
        wp_cache_delete('widget_recent_posts', 'widget');
    }

    function form( $instance ) {
        $title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : __('Recent Posts', 'thesame');
        $number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
        $show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : true;
        ?>
        <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'thesame' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

        <p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:' ); ?></label>
            <input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>

        <p><input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
            <label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Display post date?', 'thesame'); ?></label></p>
    <?php
    }
}

// Creating new widget "Contact Us"
class Contact_Us_Widget extends WP_Widget
{
    // Initialization parameters of new widget
    public function __construct()
    {
        parent::__construct("contact-us-widget", __("[The Same] Contact Us Widget", 'thesame'),
            array("description" => _("Widget, that shows Contact Us content.")));
    }

    // Function that constructs widget on site page
    function widget($args, $instance)
    {
        // Getting arguments
        extract($args);
        echo $before_widget;
        // Display the widget title
        echo $before_title . $instance['title'] . $after_title;
        ?>
        <ul class="list_contact page_text">
            <li class="phone"><?php echo $instance['phones'] ?></li>
            <li class="email"><a href="mailto:<?php echo $instance['email'] ?>"><?php echo $instance['email'] ?></a></li>
            <li class="address"><?php echo $instance['address'] ?></li>
        </ul>
        <?php
        // Loop of parsing 'thesame' posts, while it's end

        echo $after_widget;
    }

    //Update the widget settings
    function update($new_instance, $old_instance)
    {
        $values = array();

        //Strip tags from title to remove HTML. Save new settings
        $values['title'] = $new_instance['title'];
        $values['phones'] = $new_instance['phones'];
        $values['email'] = $new_instance['email'];
        $values['address'] = $new_instance['address'];

        return $values;
    }


    function form($instance)
    {

        //Set up some default widget settings.
        $defaults = array('title' => __('Contact Us', 'thesame'), 'phones' => '', 'email' => '', 'address' => '');
        $instance = wp_parse_args((array)$instance, $defaults);

        //Get widget fields values
        if (!empty($instance)) {
            $title = $instance['title'];
            $phones = $instance['phones'];
            $email = $instance['email'];
            $address = $instance['address'];
        }

        // Get widget field ID and name
        $tableId = $this->get_field_id('title');
        $tableName = $this->get_field_name('title');
        ?>
        <!-- html layout widget -->
        <label for="<?php echo $tableId ?>"><?php echo __('Title:', 'thesame') ?></label><br>
        <input id="<?php echo $tableId ?>" type="text" name="<?php echo $tableName ?>" value="<?php echo $title ?>"><br>

        <?php
        // Get widget field ID and name
        $tableId = $this->get_field_id('phones');
        $tableName = $this->get_field_name('phones');
        ?>
        <br><label for="<?php echo $tableId ?>"><?php echo __('Phones:', 'thesame') ?></label><br>
        <textarea name="<?php echo $tableName ?>" id="<?php echo $tableId ?>" cols="35" wrap="soft" placeholder="+38(xxx) xxx-xx-xx"><?php echo $phones ?></textarea><br><br>


        <?php
        // Get widget field ID and name
        $tableId = $this->get_field_id('email');
        $tableName = $this->get_field_name('email');
        ?>
        <!-- html layout widget -->
        <label for="<?php echo $tableId ?>"><?php echo __('E-mail:', 'thesame') ?></label><br>
        <input id="<?php echo $tableId ?>" type="text" size="40" name="<?php echo $tableName ?>" value="<?php echo $email ?>"><br><br>

        <?php
        // Get widget field ID and name
        $tableId = $this->get_field_id('address');
        $tableName = $this->get_field_name('address');
        ?>
        <!-- html layout widget -->
        <label for="<?php echo $tableId ?>"><?php echo __('Address:', 'thesame') ?></label><br>
        <input id="<?php echo $tableId ?>" type="text" size="40" name="<?php echo $tableName ?>" value="<?php echo $address ?>"><br><br>
    <?php

    }
}


/**
 * Portfolio Widget Class
 */
class portfolio_widget extends WP_Widget {

    /** constructor -- name this the same as the class above */
    function portfolio_widget() {
        parent::WP_Widget(false, $name = '[The Same] Portfolio');
    }

    /** @see WP_Widget::widget -- do not rename this */
    function widget($args, $instance) {
        extract( $args );
        $title 		= apply_filters('widget_title', $instance['title']); // the widget title
        $count 	= $instance['count']; // the number of categories to show
        $page 	= $instance['page']; // the taxonomy to display

        $pageId = get_page_by_title($page);
        $portfolio_url = get_page_link($pageId);

        echo $before_widget;
        // Getting data of posts with cat "portfolio" from database
        $args = array(
            'post_type' => 'portfolio',
            'posts_per_page' => $count,
        );
        $loop = new WP_Query( $args );
        // if count of posts is greater then 0, starting to print posts content on page
        if ($loop->post_count) { ?>
            <div class="portfolio">
                <p class="all_projects"><a href="<?php echo $portfolio_url ?>"><?php _e('View all projects', 'thesame'); ?></a></p>
                <?php if ( $title ) { echo $before_title . $title . $after_title; } ?>
                <div class="columns">
                    <?php
                    while ($loop->have_posts()) :
                        // Goto next post
                        $loop->the_post();
                        if (empty($image = get_field("portfolio-thumbnail"))) {
                            $thumb = $img = esc_url( get_theme_mod( 'portfolio-homepage-thumbnail-default', get_template_directory_uri() . "/gfx/portfolio-homepage-thumbnail-default.jpg" ) );
                        } else {
                            $thumb = $image['sizes']['gallery-homepage-thumbnail-thesame'];
                            $img = $image['url'];
                        }
                        ?>
                        <div class="column column25">
                            <a href="<?php echo $img; ?>" class="image lightbox" data-rel="prettyPhoto[gallery]">
                                        <span class="inside">
                                            <img src="<?php echo $thumb; ?>" alt="" />
                                            <span class="caption"><?php echo mb_substr( strip_tags( get_the_excerpt() ), 0, 20 ) . "..."; ?></span>
                                        </span>
                                <span class="image_shadow"></span>
                            </a>
                        </div>
                    <?php endwhile; ?>
                    <div class="clear"></div>
                </div>
            </div>
    <?php }
        wp_reset_query();
        echo $after_widget;
    }

    /** @see WP_Widget::update -- do not rename this */
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['count'] = strip_tags($new_instance['count']);
        $instance['page'] = $new_instance['page'];
        return $instance;
    }

    /** @see WP_Widget::form -- do not rename this */
    function form($instance) {
        //Set up some default widget settings.
        $defaults = array('title' => __('Portfolio', 'thesame'), 'count' => 4, 'page' => '');
        $instance = wp_parse_args((array)$instance, $defaults);

        $title 		= esc_attr($instance['title']);
        $count		= esc_attr($instance['count']);
        $page	    = esc_attr($instance['page']);
        $pages = get_pages();
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'thesame'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('count'); ?>"><?php _e('Number of portfolio to display', 'thesame'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>" type="text" value="<?php echo $count; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('page'); ?>"><?php _e('Choose page to link to:', 'thesame'); ?>
                <select name="<?php echo $this->get_field_name('page'); ?>" id="<?php echo $this->get_field_name('page'); ?>" class="widefat">
                    <?php foreach ($pages as $option) { ?>
                        <option value="<?php echo $option->post_title; ?>" <?php echo $page == $option->post_title ? 'selected="selected"' : ''; ?>><?php echo $option->post_title; ?></option>
                    <?php } ?>
                </select>
            </label>
        </p>
    <?php
    }
} // end class portfolio_widget

?>