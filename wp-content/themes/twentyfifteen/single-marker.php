<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 *
 * Template Name: Markers
 *
 */

get_header(); ?>
<style type="text/css">
	.box{
		width: 160px;
		height: 40px;
		background-color: lavender;
		border: solid 1px silver;
		box-shadow: 0px 3px 5px 1px silver;
		display: inline-block;
		vertical-align: middle;
		border-radius: 25px;
		border-width: 0px;
	}
</style>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php

		// Pre-process "marker" post content to append some data
		add_filter('the_content','single_marker_color_display');
		function single_marker_color_display($content)
		{
			$color = get_field( "Color" );
			$image = types_render_field("image", array("raw"=>"false"));
			$html = "My Color is: <div class='box' style='background-color:" . $color . "'></div><hr>Attached image:".$image;
			$content = $html . $content;
			return $content;
		}


		// Start the loop.
		while ( have_posts() ) : the_post();

			/*
			 * Include the post format-specific template for the content. If you want to
			 * use this in a child theme, then include a file called called content-___.php
			 * (where ___ is the post format) and that will be used instead.
			 */
			get_template_part( 'content', get_post_format() );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

			// Previous/next post navigation.
			the_post_navigation( array(
					'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'twentyfifteen' ) . '</span> ' .
							'<span class="screen-reader-text">' . __( 'Next post:', 'twentyfifteen' ) . '</span> ' .
							'<span class="post-title">%title</span>',
					'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'twentyfifteen' ) . '</span> ' .
							'<span class="screen-reader-text">' . __( 'Previous post:', 'twentyfifteen' ) . '</span> ' .
							'<span class="post-title">%title</span>',
			) );

			// End the loop.
		endwhile;
		?>

	</main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
