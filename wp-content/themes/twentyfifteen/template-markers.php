<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 *
 * Template Name: Markers
 */

get_header(); ?>
<style type="text/css">
	.box{
		width: 160px;
		height: 40px;
		background-color: lavender;
		border: solid 1px silver;
		box-shadow: 0px 3px 5px 1px silver;
		display: inline-block;
		vertical-align: middle;
		border-radius: 25px;
		border-width: 0px;
	}
</style>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php
			$args = array(
					'post_type' => 'marker',
					'publish' => true,
					'paged' => get_query_var('paged'),
			);

			query_posts($args);

			if ( have_posts() ) : ?>

			<?php if ( is_home() && ! is_front_page() ) : ?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
			<?php endif; ?>

			<?php

			// Pre-process "marker" post content to append some data
			add_filter('the_content','marker_color_display');
			function marker_color_display($content)
			{
				$color = get_field( "Color" );
				$image = types_render_field("image", array("raw"=>"false"));
				$html = "My Color is: <div class='box' style='background-color:" . $color . "'></div><hr>Attached image:".$image;
				$content = $html . $content;
				return $content;
			}

			// Start the loop.
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'content', get_post_format() );

				// End the loop.
			endwhile;

			// Previous/next page navigation.
			the_posts_pagination( array(
					'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
					'next_text'          => __( 'Next page', 'twentyfifteen' ),
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
			) );
		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'content', 'none' );

		endif;
		?>
	</main><!-- .site-main -->
</div><!-- .content-area -->
<?php get_footer(); ?>
